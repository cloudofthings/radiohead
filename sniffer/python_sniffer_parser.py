#!/usr/bin/env python3

import serial
import struct
import sys

def pretty_print_byte_string(byte_str):
	byte_count = 0
	for byte in byte_str.strip():
		byte_count += 1
		print("0x{:02x} ".format(int(byte)), end='')
		if (byte_count + 1) % 16 == 0:
			print()

def print_header(buff):
	sent_to = int(buff[0])
	sent_from  = int(buff[1])
	sent_id = int(buff[2])
	sent_flag = int(buff[3])
	print("to {0}\nfrom: {1}\nmsg_id:{2}\nflags:{3:02x}".format(sent_to, sent_from, sent_id, sent_flag))

def print_origin_and_msg_type(buff):
	identifier_str = b'COTMSG'
	pos = buff.find(identifier_str)
	app_buff = buff[pos + len(identifier_str):]
	orig_id = struct.unpack('<I', app_buff[0:4])
	command = struct.unpack('<I', app_buff[4:8])
	print("orig id: {}\ncommand: {}\n".format(orig_id, command))

if len(sys.argv) < 3:
	print("Syntax: {} [serial port, no /dev/tty] [key]".format(sys.argv[0]))
	exit(-1)
serial_port="/dev/tty{}".format(sys.argv[1])
#serial_port="/dev/pts/{}".format(sys.argv[1])
key=sys.argv[2]
if len(key) < 32:
	print("key too short")
	exit(-1)
elif len(key) > 32:
	print("key too long")
	exit(-1)

#import pdb;pdb.set_trace()
sobj = serial.Serial(port=serial_port, baudrate=9600,timeout=0)
for i in range(0,32):
	byte = key[i].encode('ascii')
	sobj.write(byte)
sobj.write(b'\r\n')
import time
time.sleep(2)
res = sobj.readall()
if res == b'':
    print("no response")
    exit(-1)
print(res.decode('utf-8'))
while(True):
	buff = sobj.readline()
	if buff == b'':
		continue
	if buff == b'STARTOFBUFF\r\n':
		print("__START_PACKET__")
		buff = sobj.readline()
		data_buff = b''
		while buff != b'ENDOFBUFF\r\n':
			data_buff += buff
			buff = sobj.readline()
		pretty_print_byte_string(data_buff)
		print()	
		print("__END_PACKET__")
		print()
		if b'COTMSG' in data_buff:
			print('---- parsed: ----')
			print_header(data_buff)
			print_origin_and_msg_type(data_buff)
			if buff == b'recv failed':
				print("RECV FAILED@@@@")


