#include <Arduino.h>
#include <Arduino.h>
#include <RH_RF95.h>                // HopeRF RFM96 Library
#include <RHEncryptedDriver.h>
#include <Speck.h>


#define IDENTIFIER_STRING "COTMSG"


#define TRANSMITTER_POWER 23        // Valid values 5 to 23 dBm for LoRa transmitter
#define COLLISION_DETECT_TIMEOUT 10000     // Number of ms to wait for CAD
#define RH_FREQUENCY 434.0           // We are at 433MHz. Change the frequency here for other modules.

#define RFM95_CS 8
#define RFM95_RST 4
#define RFM95_INT 3

#define KEY "000000000000000000000000000000000"

uint8_t current_key[16] = {0};
RH_RF95 rawDriver(RFM95_CS, RFM95_INT);
Speck   myCipher;   // Instantiate Speck block ciphering
RHEncryptedDriver driver(rawDriver, myCipher);

void sniffLoraPacket();

void setup() {
  pinMode(RFM95_RST, OUTPUT);
  digitalWrite(RFM95_RST, HIGH); // start HopeRF chip with !RESET pin high
  Serial.begin(9600);
  digitalWrite(RFM95_RST, LOW);
  delay(10);
  digitalWrite(RFM95_RST, HIGH);
  delay(10);
 
  myCipher.setKey(current_key, sizeof(current_key));
  if (!rawDriver.setFrequency(RH_FREQUENCY)) {
    Serial.println("failed to set frequency");
  }
  rawDriver.setPromiscuous(true);
  rawDriver.setTxPower(TRANSMITTER_POWER, false);
  rawDriver.setCADTimeout(COLLISION_DETECT_TIMEOUT);
  driver.setThisAddress(120);
  if(!driver.init()){
    Serial.println("fuck me, i've failed u my loard!");
  }

}



void loop() {
  sniffLoraPacket();
  if(! Serial.available()) {
    return;
  }
  String temp = Serial.readString();
  temp.trim();
  if(temp.length() == 32) {
    uint32_t oneByte = 0;
    boolean keepLooping = true;
    for(int i = 0; i < temp.length() && keepLooping == true; i+= 2) {
      int res = sscanf(temp.substring(i, i+2).c_str(), "%x", &oneByte);
      if(res != 1) {
        Serial.print(F("error reading char:"));
        Serial.println(temp.substring(i, i+1).c_str());
        keepLooping = false;
      }
      Serial.print(F("parsed one byte: "));Serial.println(oneByte, HEX);
      uint8_t realOneByte = (uint8_t)oneByte;
      current_key[i/2] = oneByte;
    }
    if(keepLooping == true) {
      myCipher.setKey(current_key, sizeof(current_key));
      Serial.println("set key: " + temp + " OK");
    }
  }else {
    Serial.print(F("got invalid len of:"));Serial.println(temp.length());
  }
}


void long2Bytes(long val, byte* bytes_array) {
  // Create union of shared memory space
  union {
    long long_variable;
    byte temp_array[4];
  } u;
  // Overwrite bytes of union with long variable
  u.long_variable = val;
  // Assign bytes to input array
  memcpy(bytes_array, u.temp_array, 4);
}

void long2BytesKeepEndian(long val, byte* bytes_array) {
  volatile uint32_t endianTest = 0x01234567;
  bool isLittleEndian = (*((uint8_t*)(&endianTest))) == 0x67;
  if(isLittleEndian) {
    uint32_t swapped = ((val>>24)&0xff) | // move byte 3 to byte 0
                ((val<<8)&0xff0000) | // move byte 1 to byte 2
                ((val>>8)&0xff00) | // move byte 2 to byte 1
                ((val<<24)&0xff000000); // byte 0 to byte 3
    val = swapped;
  }
  long2Bytes(val, bytes_array);
}

bool stringOf32HexToByteArrayKeepEndian(String &asString, byte *outBuffer) {
  if(asString.length() != 32) {
    Serial.print("bad length:");Serial.println(asString.length());
    return false;
  }
  volatile uint32_t val1, val2, val3, val4;
  int assigned = sscanf(asString.c_str(), "%8lx%8lx%8lx%8lx", &val1, &val2, &val3, &val4);
  if(assigned != 4) {
    Serial.print("bad string, assigned:");Serial.println(assigned);
    return false;
  }
  long2BytesKeepEndian(val1, &outBuffer[0]);
  long2BytesKeepEndian(val2, &outBuffer[4]);
  long2BytesKeepEndian(val3, &outBuffer[8]);
  long2BytesKeepEndian(val4, &outBuffer[12]);
  return true;
}


void sniffLoraPacket() {
  if(!driver.available()) {
    return;
  }
  uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];     // Buffer for incoming and outgoing LoRa messages
  uint8_t len = RH_RF95_MAX_MESSAGE_LEN;
  if(driver.recv(buf, &len)) {
    Serial.println("STARTOFBUFF");
    Serial.write(buf, len);
    Serial.println();
    Serial.println("ENDOFBUFF");
  } else {
    Serial.println("recv failed");
  }
}
