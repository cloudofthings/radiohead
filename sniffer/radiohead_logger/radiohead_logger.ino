// -*- mode: C++ -*-

#include <RHMesh.h>                 // RadioHead Mesh Library
#include <RH_RF95.h>                // HopeRF RFM96 Library
#include <RHEncryptedDriver.h>      // Encryption for RadioHead
#include <Speck.h>                  // Symmetric encryption algorithm. Can be changed to Tiny if needed
#include <SPI.h>                    // Communication with HopeRF module
                                    //  we use flash to store persistent vars. Has a drawback of 30,000 write operations limit
#include <FlashPersistor.h>
#include <converters.h>

RH_RF95 rawDriver(8, 3); // Adafruit Feather M0 with RFM95 
Speck   myCipher;   // Instantiate Speck block ciphering
RHEncryptedDriver driver(rawDriver, myCipher);
// Class to manage message delivery and receipt, using the driver declared above
RHMesh manager(driver, nullptr, 1);
uint8_t encryptKey[16] = {0};

//String keyAsString = "deadbeefbaddeeddeadbeefbaddeed12";
String keyAsString = "0123456789ABCDEF0123456789ABCDEF";
bool res = stringOf32HexToByteArrayKeepEndian(keyAsString, encryptKey);

void setup() 
{

  Serial.begin(9600);
  while (!Serial) ; // Wait for serial port to be available
  if (!manager.init())
    Serial.println("init failed");
  rawDriver.setTxPower(23, false);
  rawDriver.setCADTimeout(10000);
  myCipher.setKey(encryptKey, sizeof(encryptKey));
}

void loop()
{
  uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
  uint8_t len = sizeof(buf);
  uint8_t from;
  uint8_t to;
  if (manager.available())
  { 
    // Should be a reply message for us now   
    if (manager.recvfromAck(buf, &len, &from, &to))
   {
      Serial.println("Got message: ");
      Serial.print("From:"); Serial.println(from);
      Serial.print("To:"); Serial.println(to);
      Serial.print(millis());
      Serial.print(":");
      RH_RF95::printBuffer("BUF:",(const uint8_t *)buf,len);
      Serial.println();
    }
  }
}


